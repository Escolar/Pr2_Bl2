package io.github.escolarprogramming.Pr2_Bl2.AG3;

import java.io.IOException;
import java.util.HashMap;

public class Table {

    public static void main(String[] args) throws IOException {
        char[] codemap = args[0].toCharArray();
        HashMap<Integer, Integer> charmap = new HashMap<>();
        int letter = 65;
        if (codemap[0] == '-') {
            for (int i = 1; i < codemap.length; i++)
                charmap.put((int) codemap[i], letter++);
        } else {
            for (char c : codemap)
                charmap.put(letter++, (int) c);
        }

        for (int code = System.in.read(); code >= 0; code = System.in.read()) {
            if (code >= 97 && code <= 122)      // Zu Großbuchstaben machen
                code -= 32;

            if (code >= 65 && code <= 90)       // Buchstaben ersetzen
                code = charmap.get(code);

            System.out.write(code);
        }
    }
}
