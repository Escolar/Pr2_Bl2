package io.github.escolarprogramming.Pr2_Bl2.AG2;

import java.io.IOException;

public class Caesar {

    public static void main(String[] args) throws IOException {
        int offset = Integer.parseInt(args[0]);
        int code = System.in.read();
        while (code >= 0) {
            if (code >= 'a' && code <= 'z')     // Zu Großbuchstaben
                code -= 32;

            if (code >= 'A' && code <= 'Z')     // Text (de)chiffrieren
                code += offset;

            if (code < 'A' && code > '!')       // Überlauf nach unten korrigieren
                code += 26;

            if (code > 'Z')                     // Überlauf nach oben korrigieren
                code -= 26;

            System.out.write(code);
            code = System.in.read();
        }
    }
}
