package io.github.escolarprogramming.Pr2_Bl2.AG1;

public class Simplifier {

    public static void main(String[] args) {
        String input = "Erster Schritt:\n"
                + "Wegfall der Großschreibung\n"
                + "Einer sofortigen Einführung steht nichts\n"
                + "mehr im Weg, zumal schon viele Grafiker\n"
                + "und Werbeleute zur Kleinschreibung\n"
                + "übergegangen sind\n";

        String output = input.toLowerCase();         // Alles zu Kleinbuchstaben
        System.out.println(output);

        input = "Zweiter Schritt:\n"
                + "Wegfall der Dehnungen und Schärfungen\n"
                + "Diese Maßnahme eliminiert schon die größte\n"
                + "Fehlerursache in der Grundschule, den Sinn oder\n"
                + "Unsinn unserer Konsonantenverdoppelung hat\n"
                + "ohnehin niemand kapiert.\n";

        output = input.toLowerCase().replaceAll("ß", "s");      // Alles zu Kleinbuchstaben und ß ersetzen

        char[] charmap = output.toCharArray();
        for (int i = 0; i < (charmap.length - 1); i++) {
            if (charmap[i] == charmap[i + 1]) {     // Doppelte Zeichen entfernen
                charmap[i] = '\u0000';
            }

            if ((charmap[i] == 'a' || charmap[i] == 'e' || charmap[i] == 'i' || charmap[i] == 'o' || charmap[i] == 'u') && charmap[i + 1] == 'h') {
                charmap[i + 1] = '\u0000';          // h nach aeiou entfernen
            }

            if (charmap[i] == 'i' && charmap[i + 1] == 'e') {
                charmap[i + 1] = '\u0000';          // e nach i entfernen
            }
        }

        output = String.valueOf(charmap);
        System.out.println(output);
    }
}
